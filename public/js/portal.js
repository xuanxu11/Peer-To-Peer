/**
 * Created by mcbird on 15/5/13.
 */
angular.bootstrap(document.getElementById('main-navigator'), ['mainNavigatorApp']);
angular.bootstrap(document.getElementById('entry'), ['entryApp']);
angular.bootstrap(document.getElementById('app-detail'), ['detailApp']);
