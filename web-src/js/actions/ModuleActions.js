var RouterContainer = require('../services/RouterContainer');
var ModuleConstant = require('../constants/ModuleConstant');
var AppDispatcher = require('../dispatchers/AppDispatcher');

module.exports = {
  gotoModule : function(moduleId){
        RouterContainer.getRouter().transitionTo('/' + moduleId);
          AppDispatcher.dispatch({
              actionType: ModuleConstant.events.CHANGE_CURRENT_MODULE,
              id: moduleId
          });
  }
};